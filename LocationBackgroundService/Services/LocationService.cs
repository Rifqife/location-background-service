﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using System.Threading;
using Android.Gms.Location;
using Android.Gms.Common.Apis;
using Android.Locations;
using static Android.Gms.Common.Apis.GoogleApiClient;
using Android.Gms.Common;
using LocationBackgroundService.Activities;
using Android.Support.V4.Content;

namespace LocationBackgroundService.Services
{
    [Service]
    public class LocationService : Service, IConnectionCallbacks, IOnConnectionFailedListener, Android.Gms.Location.ILocationListener
    {
        private bool mRequestingLocationUpdates = false;
        private LocationRequest mLocationRequest;
        private GoogleApiClient mGoogleApiClient;
        private Location mLastLocation;

        private static int updateInterval = 3000;
        private static int fastestInterval = 1000;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            BuildGoogleApiClient();
            CreateLocationRequest();

            DisplayStartNotification();

            return StartCommandResult.Sticky;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            StopLocationUpdates();

            mGoogleApiClient.Disconnect();
            if (!mGoogleApiClient.IsConnected)
            {
                var toastHandler = new Handler();
                toastHandler.Post(() => {
                    Toast.MakeText(this, "GoogleAPI has disconnected.", ToastLength.Long).Show();
                });
            }
            
            DisplayStopNotification();

            var stopIntent = new Intent(LocationActivity.receiverFilter);
            stopIntent.PutExtra("stop", "stop");
            LocalBroadcastManager.GetInstance(this).SendBroadcast(stopIntent);
        }

        public void BuildGoogleApiClient()
        {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                .AddConnectionCallbacks(this)
                .AddOnConnectionFailedListener(this)
                .AddApi(LocationServices.API).Build();
            mGoogleApiClient.Connect();
        }

        private void CreateLocationRequest()
        {
            mLocationRequest = new LocationRequest();
            mLocationRequest.SetInterval(updateInterval);
            mLocationRequest.SetFastestInterval(fastestInterval);
            mLocationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
        }

        private void DisplayLocation()
        {
            mLastLocation = LocationServices.FusedLocationApi.GetLastLocation(mGoogleApiClient);
            if (mLastLocation != null)
            {
                double lat = mLastLocation.Latitude;
                double lng = mLastLocation.Longitude;

                Log.Debug("LatLng: ", string.Format("{0}, {1}", lat, lng));

                var lastLocation = new Intent(LocationActivity.receiverFilter);
                lastLocation.PutExtra("lat", lat);
                lastLocation.PutExtra("lng", lng);
                LocalBroadcastManager.GetInstance(this).SendBroadcast(lastLocation);
            }
            else
            {
                var toastHandler = new Handler();
                toastHandler.Post(() => {
                    Toast.MakeText(this, "Couldn't get current location.", ToastLength.Long).Show();
                });
            }
        }

        private void StartLocationUpdates()
        {
            mRequestingLocationUpdates = true;
            LocationServices.FusedLocationApi.RequestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        private void StopLocationUpdates()
        {
            mRequestingLocationUpdates = false;
            LocationServices.FusedLocationApi.RemoveLocationUpdates(mGoogleApiClient, this);
        }

        public void DisplayStartNotification()
        {
            Notification.Builder builder = new Notification.Builder(this)
                .SetContentTitle("Location Service Notification")
                .SetContentText("Location Service has started successfully.")
                .SetSmallIcon(Resource.Drawable.notification_icon);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
                GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }

        public void DisplayStopNotification()
        {
            Notification.Builder builder = new Notification.Builder(this)
                .SetContentTitle("Location Service Notification")
                .SetContentText("Location Service has stopped.")
                .SetSmallIcon(Resource.Drawable.notification_icon);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
                GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }

        public void OnConnected(Bundle connectionHint)
        {
            StartLocationUpdates();
        }

        public void OnConnectionSuspended(int cause)
        {
           
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            BuildGoogleApiClient();
        }

        public void OnLocationChanged(Location location)
        {
            var lat = location.Latitude.ToString();
            var lng = location.Longitude.ToString();

            var toastHandler = new Handler();
            toastHandler.Post(() => {
                Toast.MakeText(this, string.Format("LatLng: {0}, {1}", lat, lng), ToastLength.Short).Show();
            });

            var lastLocation = new Intent(LocationActivity.receiverFilter);
            lastLocation.PutExtra("lat", lat);
            lastLocation.PutExtra("lng", lng);
            LocalBroadcastManager.GetInstance(this).SendBroadcast(lastLocation);
        }
    }
}