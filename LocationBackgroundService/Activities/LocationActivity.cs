﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LocationBackgroundService.Services;
using Android.Util;
using Android.Support.V4.Content;

namespace LocationBackgroundService.Activities
{
    [Activity(Label = "LocationActivity", MainLauncher = true)]
    public class LocationActivity : Activity
    {
        Button btnStartService, btnStopService;
        TextView txtLatitude, txtLongitude;

        LastLocationReceiver mLastLocationReceiver;

        public const string receiverFilter = "LocationBackgroundService.Activities.LocationActivity";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.locationactivity_view);

            btnStartService = FindViewById<Button>(Resource.Id.btnStartService);
            btnStopService = FindViewById<Button>(Resource.Id.btnStopService);
            txtLatitude = FindViewById<TextView>(Resource.Id.txtLatitude);
            txtLongitude = FindViewById<TextView>(Resource.Id.txtLongitude);

            mLastLocationReceiver = new LastLocationReceiver(this);
            LocalBroadcastManager.GetInstance(this).RegisterReceiver(mLastLocationReceiver, new IntentFilter(receiverFilter));

            btnStartService.Click += BtnStartService_Click;
            btnStopService.Click += BtnStopService_Click;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            UnregisterReceiver(mLastLocationReceiver);
        }

        private void BtnStartService_Click(object sender, EventArgs e)
        {
            StartService(new Intent(this, typeof(LocationService)));
        }

        private void BtnStopService_Click(object sender, EventArgs e)
        {
            StopService(new Intent(this, typeof(LocationService)));
        }

        [IntentFilter(new[] { receiverFilter })]
        public class LastLocationReceiver : BroadcastReceiver
        {
            LocationActivity locationActivity;

            public LastLocationReceiver(LocationActivity locationActivity) { this.locationActivity = locationActivity; }

            public override void OnReceive(Context context, Intent intent)
            {                 
                if (intent.GetStringExtra("lat") != null && intent.GetStringExtra("lng") != null)
                {
                    string lat = intent.GetStringExtra("lat");
                    string lng = intent.GetStringExtra("lng");

                    Log.Debug("Receiver", string.Format("ReceiverLatLng: {0}, {1}", lat, lng));

                    locationActivity.txtLatitude.Text = lat;
                    locationActivity.txtLongitude.Text = lng;
                }
                
                if (intent.GetStringExtra("stop") != null)
                {
                    locationActivity.txtLatitude.Text = "0";
                    locationActivity.txtLongitude.Text = "0";
                }
            }
        }
    }
}